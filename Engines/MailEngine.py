#!/usr/bin/python3

"""
UI - Contains the DesQ Mail UI

###================ Program Info ================###
    Module Name : UI
    Version : 1.0.0
    Platform : Linux/Unix
    Requriements :
        Must :
            modules os, sys, PyQt5, QWebEngine
        Optional :
            module readline
    Python Version : Python 3.7 or higher
    Author : Britanicus
    Email : marcusbritanicus@gmail.com
    License : GPL version 3
###==============================================###
"""

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

    #
    # Copyright 2020 Britanicus <marcusbritanicus@gmail.com>
    #

    #
    # This program is free software; you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation; either version 2 of the License, or
    # (  at your option  ) any later version.
    #

    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.
    #

    #
    # You should have received a copy of the GNU General Public License
    # along with this program; if not, write to the Free Software
    # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    # MA 02110-1301, USA.
    #

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

import os, sys
from os.path import exists

from PyQt5.QtCore import *

from imapclient import IMAPClient
import imaplib

from email.header import decode_header

# Life time (in ms) Fixed at 10s
LIFE_TIME = 10 * 1000

# INBOX check time (in ms)
CHECK_TIME_INBOX = 10 * 1000

# MAIL check time (in ms)
CHECK_TIME_MAIL = 5 * 60 * 1000

def write( msg ):
    print( msg, end = "" )
    sys.stdout.flush()

class PostMaster( QThread ):
    """
    Engine to fetch mails, sync mails between server and local maildir and so on
    """

    def __init__( self, server, port, username, password, maildir ):
        """
        Initializer
        """

        super( PostMaster, self ).__init__()

        # Maildir
        self.mailDir = maildir[:]

        # Always use UID for mails
        write( "Connecting to server... " )
        self.imap = IMAPClient( server, port, use_uid = True )
        write( "[DONE]\n" )

        # Perform login
        write( "Logging in... " )
        response = self.imap.login( username, password )

        # Automatically set the folder to INBOX, go idle and check mail every 10s or so.
        if not response.endswith( b"(Success)" ):
            write( "[FAILED]\n" )
            self.imap = None

        else:
            write( "[DONE]\n" )

        self.mode = "NONE"

    def synchronize( self, folder = "" ):
        """
        Synchronize the mail between the server and local maildir
        """

        if not self.imap:
            return

        # First list all the folders
        folders = self.imap.list_folders()

        # If we want to sync only a particular folder
        # Remove all folders other than @folder
        if folder:
            for fspec in folders:
                # If fpec does not represent the folder, remove it from the list
                if ( fspec[ -1 ] != folder ):
                    folders.pop( folders.index( fspec ) )

            if not len( folders ):
                return;

        # Iterate through all folders
        for fspec in folders:
            # Create the folder in the local maildir.
            QDir( self.mailDir ).mkpath( fspec[ -1 ] )

            try:
                # Now try to select this folder
                response = self.imap.select_folder( fspec[ -1 ] )

            # If the folder is not selectable
            except:
                # Continue to next folder
                continue

            # Folder Info: .mboxinfo in ini format
            # This will also contain all the email headers
            mboxinfo = QSettings( f"{self.mailDir}/{fspec[ -1 ]}/.mboxinfo", QSettings.IniFormat )

            # Check the UIDVALIDITY; if UIDVALIDITY has changed, discard all emails in the mailbox
            if response[ b'UIDVALIDITY' ] != mboxinfo.value( "UIDVALIDITY", type = int ):
                print( f'Cleanup the mailbox {fspec[ -1 ]}' )
                mboxinfo.clear()
                for eml in os.listdir( f"{self.mailDir}/{fspec[ -1 ]}/" ):
                    if eml.endswith( ".eml" ) :
                        try:
                            os.unlink( eml )
                        except FileNotFoundError:
                            pass

                mboxinfo.setValue( "UIDVALIDITY", response[ b'UIDVALIDITY' ] )
                mboxinfo.setValue( "UIDs", [] )

            # Get existing UIDs, we need to delete these if not in server
            UIDs = mboxinfo.value( "UIDs", defaultValue = [] )
            if type( UIDs ) == type( [] ):
                UIDs = [ int( i ) for i in UIDs ]

            else:
                UIDs = []

            # Get all UIDs from the server
            serverUIDs = self.imap.search()
            serverUIDs.sort()
            serverUIDs.reverse()
            write( f"Synchronizing the folder {fspec[-1]} containing {len(serverUIDs)} mails..." )

            # Delete stale emails
            for uid in UIDs:
                if uid not in serverUIDs:
                    # Remove from the list
                    UIDs.pop( UIDs.index( uid ) )

                    # Remove the header
                    mboxinfo.remove( f"{uid}" )

                    # Remove fetched email
                    if exists( f"{uid}.eml" ):
                        try:
                            os.unlink( f"{uid}.eml" )
                        except FileNotFoundError:
                            pass

            # Check the list of UIDs that need to be fetched
            fetchUIDs = []
            for uid in serverUIDs:
                if not mboxinfo.contains( f"{uid}/Subject" ):
                    fetchUIDs.append( uid )

            # If our database is up to date,
            # then continue to next mailbox
            if not fetchUIDs:
                print( "[DONE]" )
                continue

            else:
                print()

            # Fetch FROM/SUBJECT/Date fields for all the mails
            write( "    Fetching header info... " )
            rmsg = self.imap.fetch( fetchUIDs, "ENVELOPE" )
            write( "[DONE]\n" )

            write( "    Updating database... " )
            for uid in fetchUIDs:
                # Add the uid to the list if it's not already there
                if uid not in UIDs:
                    UIDs.append( uid )
                    UIDs.sort()
                    UIDs.reverse()
                    mboxinfo.setValue( "UIDs", UIDs )

                # Get the mail header and parse it
                msg =  rmsg[ uid ][ b"ENVELOPE" ]

                # Save the headers
                mboxinfo.beginGroup( f"{uid}" )

                # Get From
                sender = msg.sender[ 0 ].name.decode() if msg.sender[ 0 ].name else ""
                if not sender:
                    sender = f"{msg.sender[ 0 ].mailbox.decode()}@{msg.sender[ 0 ].host.decode()}"

                # Clean up UTF-8 names
                if sender.startswith( "=?" ):
                    sender, charset = decode_header( sender )[ 0 ]
                    sender = sender.decode( charset )

                subject = msg.subject.decode() if msg.subject else "(No subject)"
                # Clean up UTF-8 names
                if subject.startswith( "=?" ):
                    subject, charset = decode_header( subject )[ 0 ]
                    subject = subject.decode( charset )

                mboxinfo.setValue( 'From', sender )
                mboxinfo.setValue( 'Subject', subject )
                mboxinfo.setValue( 'Date', msg.date.ctime() )
                mboxinfo.setValue( 'CC', True if msg.cc else False )
                mboxinfo.setValue( 'BCC', True if msg.bcc else False )
                mboxinfo.endGroup()

            else:
                print( "[DONE]" )

        write( "Logging out... " )
        self.imap.logout()
        print( "[DONE]" )

    def run( self ):

        if self.mode == "SYNC":
            self.synchronize()

class PostalService( QObject ):
    """Engine to fetch and organize emails"""

    selectFailed = pyqtSignal( str )
    resetFolder = pyqtSignal( str )
    fetchFailed = pyqtSignal( str, int )
    mailFetched = pyqtSignal( str, int )

    def __init__( self, server, port, username, password, maildir ):
        """
        Initializer
        """

        super( PostalService, self ).__init__()

        self.server = server[:]
        self.port = port
        self.username = username[:]
        self.password = password[:]
        self.maildir = maildir[:]

        self.currentMails = 0;

        # Main IMAP Agent - We will be waiting for new mail on this
        self.imap = None

        # Terminate mail sync
        self.terminate = False

        # Life timer
        self.lifeTimer = QBasicTimer()

        # Fetch mail from INBOX every n seconds (Default: n = 10)
        self.inboxCheckTimer = QBasicTimer()

        # Fetch mail from all folders every m minutes (Default: m = 5)
        self.mailCheckTimer = QBasicTimer()

        # Current state of remove server
        # This holds the number of mails in each remote mailbox
        self.state = {}

    def login( self ):
        """
        Perform an IMAP login and get ready to fetch mails
        """

        # Always use UID for mails
        self.imap = IMAPClient( self.server, self.port, use_uid = True )

        # Perform login
        response = self.imap.login( self.username, self.password )

        # Automatically set the folder to INBOX, go idle and check mail every 10s or so.
        if response.endswith( b"(Success)" ):
            self.sync()

            # Now query all the folders for number of mails and save it in state
            folders = self.imap.list_folders()
            for fspec in folders:
                try:
                    # Now try to select this folder
                    response = self.imap.select_folder( fspec[ -1 ] )

                    # If it works, save the UIDVALIDITY and number of mails in the folder
                    self.state[ fspec[ -1 ] ] = [ response[ b'UIDVALIDITY' ], response[ b'EXISTS' ] ]

                # If the folder is not selectable
                except:
                    # Continue to next folder
                    continue

            # Life Timer
            self.lifeTimer.start( LIFE_TIME, self )

            # Start INBOX check timer
            self.inboxCheckTimer.start( CHECK_TIME_INBOX, self )

            # Start MAIL check timer
            self.mailCheckTimer.start( CHECK_TIME_MAIL, self )

            return True

        else:
            self.imap = None
            return False

    def sync( self, folder = "" ):
        """
        Synchronize mails from server and local storage
        """

        # If the mail agent failed to login, we cannot login either
        if self.imap == None:
            return

        self.synchronizer = PostMaster( self.server, self.port, self.username, self.password, self.maildir )
        self.synchronizer.mode = "SYNC"
        self.synchronizer.start()

    def fetch( self, mails ):
        """
        Fetch thefull mail content of the specified mails

        @mails wil have the following format
        mails = [
            [ foldername1, (uid1, uid2, uid3, uid4, uid5...) ],
            [ foldername2, (uid1, uid2, uid3, uid4, uid5...) ],
            [ foldername3, (uid1, uid2, uid3, uid4, uid5...) ],
        ]

        We can fetch all the content of mails in a folder, but that might be slow.
        We will be fetching the mail content one by one
        """

        for mail in mails:
            # If we have already fetched this mail, skip re-acquiring it.
            for uid in mail[ 1 ]:
                if exists( f"{self.maildir}/{mail[ 0 ]}/{uid}.eml" ):
                    self.mailFetched.emit( mail[ 0 ], uid )
                    continue

            try:
                response = self.imap.select_folder( mail[ 0 ] )

                # Check if the UIDVALIDITY has changed
                if self.state[ mail[ 0 ] ][ 0 ] != response[ b'UIDVALIDITY' ]:
                    self.state[ mail[ 0 ] ] = [ response[ b'UIDVALIDITY' ], response[ b'EXISTS' ] ]
                    self.sync( mail[ 0 ] )

                    ## Reset folder Signal
                    self.resetFolder.emit( mail[ 0 ] )

                    continue

                for uid in mail[ 1 ]:

                    rmsg = self.imap.fetch( uid, "RFC822" )
                    if not rmsg.keys():
                        print( f"Failed to fetch {uid}" )
                        self.fetchFailed.emit( mail[ 0 ], uid )
                        continue

                    f = open( f"{self.maildir}/{mail[ 0 ]}/{uid}.eml", 'wb' )
                    f.write( rmsg[ uid ][ b'RFC822' ] )
                    f.close()

                    self.mailFetched.emit( mail[ 0 ], uid )

            except imaplib.IMAP4.error as err:
                print( f"Failed to select {mail[0]}", err )
                self.selectFailed.emit( mail[ 0 ] )
                continue

            except Exception as e:
                print( f"Failed to get mail from {mail[ 0 ]}", e )
                self.selectFailed.emit( mail[ 0 ] )
                continue

    def timerEvent( self, tEvent ):
        """
        Keep our IMAP agent alive
        """

        # Life Timer - Keep IMAP connection alive
        if self.lifeTimer.timerId() == tEvent.timerId():
            # Just to let the server know we're still in touch with it
            self.imap.noop()

        # Update the status of the inbox
        elif self.inboxCheckTimer.timerId() == tEvent.timerId():

            response = self.imap.select_folder( "INBOX" )

            # UIDVALIDITY has changed (Bad server): Sync the inbox
            if self.state[ "INBOX" ][ 0 ] != response[ b'UIDVALIDITY' ]:
                self.state[ "INBOX" ] = [ response[ b'UIDVALIDITY' ], response[ b'EXISTS' ] ]

                self.resetFolder( "INBOX" )
                self.sync( "INBOX" )
                return

            # Mail deleted or received: Sync the inbox
            if self.state[ "INBOX" ][ 1 ] != response[ b'EXISTS' ]:
                self.state[ "INBOX" ][ 1 ] = response[ b'EXISTS' ]
                self.sync( "INBOX" )

        # Update all mails
        elif self.inboxCheckTimer.timerId() == tEvent.timerId():
            # Query all the folders for number of mails and save it in state
            folders = self.imap.list_folders()
            for fspec in folders:
                try:
                    # Now try to select this folder
                    response = self.imap.select_folder( fspec[ -1 ] )

                    # UIDVALIDITY has changed (Bad server): Sync the folder
                    if self.state[ fspec[ -1 ] ][ 0 ] != response[ b'UIDVALIDITY' ]:
                        self.state[ fspec[ -1 ] ] = [ response[ b'UIDVALIDITY' ], response[ b'EXISTS' ] ]

                        self.resetFolder( fspec[ -1 ] )
                        self.sync( fspec[ -1 ] )
                        return

                    # If it works, save the number of mails in the folder
                    self.state[ fspec[ -1 ] ] = response[ b'EXISTS' ]

                # If the folder is not selectable
                except:
                    self.selectFailed( fspec[ -1 ] )
                    # Continue to next folder
                    continue

if __name__ == '__main__' :

    from PyQt5.QtWidgets import *

    app = QApplication( sys.argv )

    uname = QInputDialog.getText(
        None,
        "DesQ Mail | GMail Username",
        "Enter your gmail username:",
        QLineEdit.Normal
    )

    paswd = QInputDialog.getText(
        None,
        "DesQ Mail | GMail Password",
        "Enter your gmail password:",
        QLineEdit.Password
    )

    if ( uname and paswd ):
        pm = PostMaster( "imap.gmail.com", 993, uname[ 0 ], paswd[ 0 ], f"/tmp/mailbox/" )
        pm.synchronize()
