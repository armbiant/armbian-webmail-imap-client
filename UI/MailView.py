#!/usr/bin/python3

"""
MailView - QWebView subclass to show the eMail

###================ Module Info ================###
    Program Name : MailView
    Version : 1.0.0
    Platform : Linux/Unix
    Requriements :
        Must :
            modules os, sys, PyQt5
        Optional :
            module readline
    Python Version : Python 3.7 or higher
    Author : Britanicus
    Email : marcusbritanicus@gmail.com
    License : GPL version 3
###==============================================###
"""

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

    #
    # Copyright 2020 Britanicus <marcusbritanicus@gmail.com>
    #

    #
    # This program is free software; you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation; either version 2 of the License, or
    # ( at your option ) any later version.
    #

    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.
    #

    #
    # You should have received a copy of the GNU General Public License
    # along with this program; if not, write to the Free Software
    # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    # MA 02110-1301, USA.
    #

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

import os, sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import email, email.header, urllib.request
from bs4 import BeautifulSoup as BS4

from Engines.EmlEngine import get_mail_contents, decode_text

class ImageFetcher( QThread ):
    """
    Fetch images from the web
    """

    fetched = pyqtSignal( str, QPixmap )
    def __init__( self ):
        """
        Class initializer
        """

        super( ImageFetcher, self ).__init__()

        self.toBeFetched = []

    def fetch( self, url ):
        """
        Start the fetching process
        """

        self.toBeFetched.append( url )

        if not self.isRunning():
            self.start()

    def run( self ):
        """
        The actual fetching
        """

        while( len( self.toBeFetched ) ):
            fetch = self.toBeFetched.pop()
            res = urllib.request.urlretrieve( fetch )
            if ( os.path.exists( res[ 0 ] ) ):
                print( "=> Fetched", fetch )
                self.fetched.emit( fetch, QPixmap( res[ 0 ] ) )

class MailView( QTextBrowser ):
    """A smooth modern interface for IMAP/SMTP eMail providers
    """

    def __init__( self ):
        """Class initializer
        """

        super( MailView, self ).__init__()
        self.setFrameStyle( QFrame.NoFrame )
        clr = self.palette().color( QPalette.Window ).darker( 110 )
        self.setStyleSheet( "background-color: %s; border: none; border-radius: 5px; padding 5px;" % clr.name() )

    def loadMail( self, path ):
        raw = open( path, 'rb' ).read()

        msg = email.message_from_bytes( raw )
        attachments = get_mail_contents( msg )

        self.contents = {}

        html = ""
        text = ""

        iFetcher = ImageFetcher()
        iFetcher.fetched.connect( self.storeImage )

        for attach in attachments:
            # If the attachement is an embedded image
            if attach.content_id and attach.type.startswith( "image/" ):
                self.contents[ attach.content_id ] = QPixmap( QImage.fromData( attach.payload ) )

            if attach.is_body == "text/html":
                html, used_charset = decode_text( attach.payload, attach.charset, 'auto' )
                soup = BS4( html, "html.parser" )

                for img in soup.find_all( "img" ):
                    iFetcher.fetch( img[ "src" ] )

                body = soup.find( "body" )
                if body:
                    html = body.decode()
                    html = html.replace( 'bgcolor=', '' ).replace( 'color=', '' ).replace( 'background-color:', '' ).replace( 'color:', '' )

            if attach.is_body == "text/plain":
                text, used_charset = decode_text( attach.payload, attach.charset, 'auto' )
                if text.startswith( "=?" ) :
                    text = decode_header( text )[ 0 ]

        if html:
            self.setHtml( html )

        elif text:
            self.setText( text )

    def storeImage( self, id, imgdata ):

        self.contents[ id ] = imgdata

    def loadResource( self, type, url ):
        if type == 2:
            name = url.toString()

            try:
                pix = self.contents[ name[4:] ] if name.startswith( "cid:" ) else self.contents[ name ]
                psize = pix.size()
                vsize = self.viewport().size() - QSize( self.verticalScrollBar().width(), 0 )
                if ( psize.width() > vsize.width() ):
                    return pix.scaled( vsize, Qt.KeepAspectRatio, Qt.SmoothTransformation )

                else:
                    return pix

            # It is being retrieved or cannot be retrieved
            except KeyError:
                pass

        # If we have some other format, we would like to know
        else:
            print( type, url.toString() )

if __name__ == '__main__' :

    app = QApplication( sys.argv )

    Gui = CardsView()
    Gui.show()

    sys.exit( app.exec_() )
